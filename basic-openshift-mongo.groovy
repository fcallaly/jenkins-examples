def projectName = <INSERT PROJECT NAME HERE>
def version = "0.0.${currentBuild.number}"
def dockerImageTag = "${projectName}:${version}"
def gitUrl = <INSERT GIT URL HERE>
def linuxHostname = <INSERT LINUX HOSTNAME HERE>

// NOTE - THIS PATH SHOULD BE CHANGED TO SUIT THE PROJECT
def smokeTestUrl = "http://${projectName}.${linuxHostname}:80/v1/employee"

pipeline {
  agent any
 
  stages {
    stage("Clone Source") {
      steps {
        git(
          url: "${gitUrl}",
          branch: "master"
        )
      }
    }

    stage('Test') {
      steps {
        sh 'chmod a+x gradlew'
        sh './gradlew test'
      }
    }

    stage('Build') {
      steps {
        sh './gradlew build'
      }
    }

    stage('Build Container') {
      steps {
        sh "docker build -t ${dockerImageTag} ."
      }
    }

    stage('Deploy Container To Openshift') {
      steps {
        sh "oc project ${projectName} || oc new-project ${projectName}"
        sh "oc get service mongo || oc new-app mongo"
        sh "oc delete all --selector app=${projectName} || echo 'Unable to delete all previous openshift resources'"
        sh "oc new-app ${dockerImageTag} -l version=${version} -e DB_HOST=mongo"
        sh "oc expose svc/${projectName} --hostname=${projectName}.${linuxHostname}"
      }
    }

    // NOTE: this is ONLY a placeholder for more complete tests
    stage('Smoke Test') {
      steps {
        // wait for deployment - could be more sophisticated
        sleep 20
        sh "curl -f -X GET ${smokeTestUrl}"
      }
    }

  }

  post {
    always {
      archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true
      archiveArtifacts artifacts: 'build/jacoco/**/*'
      archiveArtifacts 'build/reports/**/*'
    }
  }
}

